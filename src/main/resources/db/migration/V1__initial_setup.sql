create table outlet
(
    id integer not null
         constraint outlet_pk
             primary key,
    last_name varchar,
    location varchar,
    outlet_name varchar,
    outlet_type varchar
);


