package nl.voorth.unilever;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static nl.voorth.unilever.jooq.Tables.*;

@Service
@Transactional
public class CsvUploadService implements UploadService
{
  private final DSLContext context;

  @Autowired
  public CsvUploadService(DSLContext context)
  {
    this.context = context;
  }

  @Override
  public void upload(MultipartFile file)
  {
    try
    {
      var inputStream = file.getInputStream();
      var inputReader = new InputStreamReader(inputStream, "UTF-8");
      var reader = new BufferedReader(inputReader);
      context
        .loadInto(OUTLET) //insert into outlet
        .onDuplicateKeyUpdate() // this will take care of deduplication
        .loadCSV(reader)
        .fields(OUTLET.ID, OUTLET.LAST_NAME, OUTLET.LOCATION, OUTLET.OUTLET_NAME, OUTLET.OUTLET_TYPE)
        .execute();
    }
    catch (Exception e)
    {
      System.out.println(e.getMessage());
      e.printStackTrace();
    }
  }

}
