package nl.voorth.unilever;

import org.springframework.web.multipart.MultipartFile;

public interface UploadService
{
  public void upload(MultipartFile file);
}
