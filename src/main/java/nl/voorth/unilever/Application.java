package nl.voorth.unilever;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableTransactionManagement
@Component
public class Application {

    public static void main(String[] args) {
        run(Application.class, args);
    }

}
